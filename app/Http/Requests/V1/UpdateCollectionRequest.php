<?php

namespace App\Http\Requests\V1;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCollectionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
        //        $user = $this->user();
        //
        //        return $user != null && $user->tokenCan('update');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        $method = $this->method();

        if ($method == 'PUT') {
            return [
                'title' => ['required'],
                'description' => ['required'],
                'target_amount' => ['required'],
                'link' => ['required'],
            ];
        } else {
            return [
                'title' => ['sometimes'],
                'description' => ['sometimes'],
                'target_amount' => ['sometimes'],
                'link' => ['sometimes'],
            ];
        }
    }

    //    protected function prepareForValidation()
    //    {
    //        if ($this->targetAmount) {
    //            $this->merge([
    //                'target_amount' => $this->targetAmount,
    //            ]);
    //        }
    //    }
}
