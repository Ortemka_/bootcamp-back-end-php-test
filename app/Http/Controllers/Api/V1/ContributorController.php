<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\V1\StoreContributorRequest;
use App\Http\Resources\V1\ContributorCollection;
use App\Http\Resources\V1\ContributorResource;
use App\Models\Contributor;

class ContributorController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return new ContributorCollection(Contributor::paginate());
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreContributorRequest $request)
    {
        return new ContributorResource(Contributor::create($request->validated()));

    }

    /**
     * Display the specified resource.
     */
    public function show(Contributor $contributor)
    {
        return new ContributorResource($contributor);
    }
}
