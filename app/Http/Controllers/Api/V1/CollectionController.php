<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\V1\AddContributorRequest;
use App\Http\Resources\V1\CollectionCollection;
use App\Http\Resources\V1\CollectionFilteredResource;
use App\Http\Resources\V1\CollectionShowResource;
use App\Models\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CollectionController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        //Checking that parameters for filtering were passed in the url

        $amountShortfall = $request->amountShortfall;
        $notClosed = $request->notClosed;

        // Option $amountShortfall and $notClosed
        // $amountShortfall - returned collections for which there is not enough such amount to close
        // $notClosed - returned collections with total contributions less than specified

        if ($amountShortfall || $notClosed) {
            $queryEnd = 'collections.target_amount - total_contributions ';
            $queryEnd .= $amountShortfall ? "<= $amountShortfall" : '';
            $queryEnd .= $notClosed ? '> 0' : '';

            $collections = DB::table('collections')
                ->select('collections.*', DB::raw('COALESCE(SUM(contributors.amount), 0) as total_contributions'))
                ->leftJoin('contributors', 'collections.id', '=', 'contributors.collection_id')
                ->groupBy('collections.id', 'collections.title', 'collections.description', 'collections.target_amount', 'collections.link')
                ->havingRaw($queryEnd)
                ->paginate();

            return CollectionFilteredResource::collection($collections);
        } else {
            return new CollectionCollection(Collection::paginate());
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(Collection $collection)
    {
        $collection->with('contributors');

        return new CollectionShowResource($collection);
    }

    /**
     * Adds a contribution to the specified collection.
     */
    public function addContributor(Collection $collection, AddContributorRequest $request)
    {
        $contributor = $request->validated();
        $collection->contributors()->create($contributor);

        return new CollectionShowResource($collection);
    }
}
