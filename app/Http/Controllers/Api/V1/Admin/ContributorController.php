<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\V1\UpdateContributorRequest;
use App\Models\Contributor;

class ContributorController extends Controller
{
    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateContributorRequest $request, Contributor $contributor)
    {
        $contributor->update($request->validated());
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Contributor $contributor)
    {
        if (! $contributor) {
            return response()->json(['message' => 'Contributor not found'], 404);
        }
        $contributor->delete();

        return response()->json(['message' => 'Contributor deleted successfully'], 200);
    }
}
