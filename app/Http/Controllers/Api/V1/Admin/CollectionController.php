<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\V1\StoreCollectionRequest;
use App\Http\Requests\V1\UpdateCollectionRequest;
use App\Http\Resources\V1\CollectionResource;
use App\Models\Collection;

class CollectionController extends Controller
{
    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreCollectionRequest $request)
    {
        return new CollectionResource(Collection::create($request->validated()));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateCollectionRequest $request, Collection $collection)
    {
        $collection->update($request->validated());
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Collection $collection)
    {
        if (! $collection) {
            return response()->json(['message' => 'Collection not found'], 404);
        }
        $collection->delete();

        return response()->json(['message' => 'Collection deleted successfully'], 200);
    }
}
