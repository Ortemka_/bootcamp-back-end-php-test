<?php

namespace Tests\Feature;

use App\Models\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ContributorTest extends TestCase
{
    use RefreshDatabase;

    public function test_saves_contributor_successfully_with_valid_data(): void
    {
        $collection = Collection::factory()->create();

        $response = $this->postJson('/api/v1/contributor', [
            'amount' => 1000,
        ]);

        $response->assertStatus(422);

        $response = $this->postJson('/api/v1/contributor', [
            'collection_id' => $collection->id,
            'user_name' => 'Name Surname',
            'amount' => 1000,
        ]);

        $response->assertStatus(201);

        $response = $this->get('/api/v1/contributor');
        $response->assertJsonFragment(['user_name' => 'Name Surname']);
    }

    public function test_saves_contributor_to_specified_collection_correctly()
    {
        $collection = Collection::factory()->create();

        $response = $this->postJson('/api/v1/collection/'.$collection->id.'/add-amount', [
            'amount' => 1000,
        ]);

        $response->assertStatus(422);

        $response = $this->postJson('/api/v1/collection/'.$collection->id.'/add-amount', [
            'user_name' => 'Name Surname',
            'amount' => 1000,
        ]);

        $response->assertStatus(200);

        $response = $this->get('/api/v1/collection/'.$collection->id);

        $response->assertStatus(200);

        $response->assertJsonCount(1, 'data.contributors');

        $response->assertJsonPath('data.contributors.0.user_name', 'Name Surname');
        $response->assertJsonPath('data.contributors.0.amount', 1000);
    }
}
