<?php

namespace Tests\Feature;

use App\Models\Collection;
use App\Models\Contributor;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CollectionsListTest extends TestCase
{
    use RefreshDatabase;

    public function test_collections_list_returns_paginated_data_correctly(): void
    {
        Collection::factory(16)->create();

        $response = $this->get('/api/v1/collection');

        $response->assertStatus(200);
        $response->assertJsonCount(15, 'data');
        $response->assertJsonPath('meta.last_page', 2);
    }

    public function test_returns_specific_collection_by_id_correctly(): void
    {
        $collection = Collection::factory()->create();

        $response = $this->get('/api/v1/collection/'.$collection->id);

        $response->assertStatus(200);

    }

    public function test_returns_specific_collection_with_contributors_data_correctly(): void
    {
        $collection = Collection::factory()->create();

        $firstContributor = Contributor::factory()->create([
            'collection_id' => $collection->id,
            'user_name' => 'Ivan Ivanov',
            'amount' => 556,
        ]);
        $secondContributor = Contributor::factory()->create([
            'collection_id' => $collection->id,
            'user_name' => 'Dima Ivanov',
            'amount' => 1145,
        ]);

        $response = $this->get('/api/v1/collection/'.$collection->id);

        $response->assertStatus(200);

        $response->assertJsonCount(2, 'data.contributors');

        //      Check first contributor data
        $response->assertJsonPath('data.contributors.0.user_name', $firstContributor->user_name);
        $response->assertJsonPath('data.contributors.0.amount', $firstContributor->amount);

        //      Check second contributor data
        $response->assertJsonPath('data.contributors.1.user_name', $secondContributor->user_name);
        $response->assertJsonPath('data.contributors.1.amount', $secondContributor->amount);
    }

    public function test_collections_list_filtered_by_amount_less_than_the_target_amount()
    {
        $firstCollection = Collection::factory()->create([
            'target_amount' => 1000,
        ]);
        $secondCollection = Collection::factory()->create([
            'target_amount' => 2000,
        ]);

        //      Contributions for first collection
        $firstContributor = Contributor::factory()->create([
            'collection_id' => $firstCollection->id,
            'amount' => 500,
        ]);
        $secondContributor = Contributor::factory()->create([
            'collection_id' => $firstCollection->id,
            'amount' => 200,
        ]);

        $firstCollectionTotalContributors = $firstContributor->amount + $secondContributor->amount;
        $firstCollectionAmountShortfall = $firstCollection->target_amount - $firstCollectionTotalContributors;

        //      Contribution for second collection
        Contributor::factory()->create([
            'collection_id' => $secondCollection->id,
            'amount' => 2000,
        ]);

        $response = $this->get('/api/v1/collection?notClosed=true');
        $response->assertStatus(200);
        $response->assertJsonCount(1, 'data');
        $response->assertJsonPath('data.0.total_contributions', $firstCollectionTotalContributors);
        $response->assertJsonPath('data.0.amount_shortfall', $firstCollectionAmountShortfall);
    }
}
