<?php

namespace Tests\Feature;

use App\Models\Collection;
use App\Models\Contributor;
use App\Models\Role;
use App\Models\User;
use Database\Seeders\RoleSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class AdminContributorTest extends TestCase
{
    use RefreshDatabase;

    public function test_updates_contributor_successfully_with_valid_data_using_patch_method(): void
    {
        $this->seed(RoleSeeder::class);
        $user = User::factory()->create();
        $user->roles()->attach(Role::where('name', 'admin')->value('id'));

        $collection = Collection::factory()->create();

        $contributor = Contributor::factory()->create([
            'collection_id' => $collection->id,
        ]);

        $response = $this->actingAs($user)->patchJson('/api/v1/contributor/'.$contributor->id, [
            'user_name' => 'Name Surname',
        ]);

        $response->assertStatus(200);

        $response = $this->get('/api/v1/contributor/'.$contributor->id);
        $response->assertJsonFragment(['user_name' => 'Name Surname']);
    }

    public function test_updates_contributor_successfully_with_valid_data_using_put_method(): void
    {
        $this->seed(RoleSeeder::class);
        $user = User::factory()->create();
        $user->roles()->attach(Role::where('name', 'admin')->value('id'));
        $collection = Collection::factory()->create();

        $contributor = Contributor::factory()->create([
            'collection_id' => $collection->id,
        ]);

        $response = $this->actingAs($user)->putJson('/api/v1/contributor/'.$contributor->id, [
            'user_name' => 'Name Surname',
        ]);

        $response->assertStatus(422);

        $response = $this->actingAs($user)->patchJson('/api/v1/contributor/'.$contributor->id, [
            'user_name' => 'Name Surname',
            'amount' => 1000,
        ]);

        $response->assertStatus(200);

        $response = $this->get('/api/v1/contributor/'.$contributor->id);
        $response->assertJsonFragment(['user_name' => 'Name Surname']);
        $response->assertJsonFragment(['amount' => 1000]);
    }

    public function test_public_user_cannot_access_deleting_contributor(): void
    {
        $collection = Collection::factory()->create();
        $contributor = Contributor::factory()->create([
            'collection_id' => $collection->id,
        ]);

        $response = $this->deleteJson('/api/v1/contributor/'.$contributor->id);

        $response->assertStatus(401);
    }

    public function test_non_admin_user_cannot_access_deleting_contributor(): void
    {
        $this->seed(RoleSeeder::class);
        $user = User::factory()->create();
        $user->roles()->attach(Role::where('name', 'basic')->value('id'));

        $collection = Collection::factory()->create();
        $contributor = Contributor::factory()->create([
            'collection_id' => $collection->id,
        ]);

        $response = $this->actingAs($user)->deleteJson('/api/v1/contributor/'.$contributor->id);

        $response->assertStatus(403);
    }

    public function test_admin_user_can_access_deleting_contributor(): void
    {
        $this->seed(RoleSeeder::class);
        $user = User::factory()->create();
        $user->roles()->attach(Role::where('name', 'admin')->value('id'));

        $collection = Collection::factory()->create();

        $contributor = Contributor::factory(5)->create([
            'collection_id' => $collection->id,
        ]);

        $response = $this->actingAs($user)->deleteJson('/api/v1/contributor/'.$contributor->last()->id);
        $response->assertStatus(200);

        $response = $this->get('/api/v1/collection/'.$collection->id);
        $response->assertStatus(200);
        $response->assertJsonCount(4, 'data.contributors');
    }
}
