<?php

namespace Tests\Feature;

use App\Models\Collection;
use App\Models\Role;
use App\Models\User;
use Database\Seeders\RoleSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class AdminCollectionTest extends TestCase
{
    use RefreshDatabase;

    public function test_public_user_cannot_access_adding_collection(): void
    {
        $response = $this->postJson('/api/v1/collection');

        $response->assertStatus(401);
    }

    public function test_non_admin_user_cannot_access_adding_collection(): void
    {
        $this->seed(RoleSeeder::class);
        $user = User::factory()->create();
        $user->roles()->attach(Role::where('name', 'basic')->value('id'));

        $response = $this->actingAs($user)->postJson('/api/v1/collection');

        $response->assertStatus(403);
    }

    public function test_saves_collection_successfully_with_valid_data(): void
    {
        $this->seed(RoleSeeder::class);
        $user = User::factory()->create();
        $user->roles()->attach(Role::where('name', 'admin')->value('id'));

        $response = $this->actingAs($user)->postJson('/api/v1/collection', [
            'title' => 'Collection for ZSU',
        ]);

        $response->assertStatus(422);

        $response = $this->actingAs($user)->postJson('/api/v1/collection', [
            'title' => 'Collection for ZSU',
            'description' => 'For 3 bayraktars',
            'target_amount' => 4000,
            'link' => 'https://uk.wikipedia.org/wiki/Bayraktar_TB2',
        ]);

        $response->assertStatus(201);

        $response = $this->get('/api/v1/collection');
        $response->assertJsonFragment(['title' => 'Collection for ZSU']);
    }

    public function test_updates_collection_successfully_with_valid_data_using_patch_method(): void
    {
        $this->seed(RoleSeeder::class);
        $user = User::factory()->create();
        $user->roles()->attach(Role::where('name', 'admin')->value('id'));
        $collection = Collection::factory()->create();

        $response = $this->actingAs($user)->patchJson('/api/v1/collection/'.$collection->id, [
            'description' => 'For 3 bayraktars',
            'target_amount' => 4000,
        ]);

        $response->assertStatus(200);

        $response = $this->get('/api/v1/collection/'.$collection->id);
        $response->assertJsonFragment(['description' => 'For 3 bayraktars']);
        $response->assertJsonFragment(['target_amount' => 4000]);
    }

    public function test_updates_collection_successfully_with_valid_data_using_put_method(): void
    {
        $this->seed(RoleSeeder::class);
        $user = User::factory()->create();
        $user->roles()->attach(Role::where('name', 'admin')->value('id'));
        $collection = Collection::factory()->create();

        $response = $this->actingAs($user)->putJson('/api/v1/collection/'.$collection->id, [
            'title' => 'Collection for ZSU',
        ]);

        $response->assertStatus(422);

        $response = $this->actingAs($user)->putJson('/api/v1/collection/'.$collection->id, [
            'title' => 'Collection for ZSU',
            'description' => 'For 3 bayraktars',
            'target_amount' => 4000,
            'link' => 'https://uk.wikipedia.org/wiki/Bayraktar_TB2',
        ]);

        $response->assertStatus(200);

        $response = $this->get('/api/v1/collection/'.$collection->id);
        $response->assertJsonFragment(['title' => 'Collection for ZSU']);
        $response->assertJsonFragment(['description' => 'For 3 bayraktars']);
        $response->assertJsonFragment(['target_amount' => 4000]);
        $response->assertJsonFragment(['link' => 'https://uk.wikipedia.org/wiki/Bayraktar_TB2']);
    }

    public function test_public_user_cannot_access_deleting_collection(): void
    {
        $collection = Collection::factory()->create();

        $response = $this->deleteJson('/api/v1/collection/'.$collection->id);

        $response->assertStatus(401);
    }

    public function test_non_admin_user_cannot_access_deleting_collection(): void
    {
        $this->seed(RoleSeeder::class);
        $user = User::factory()->create();
        $user->roles()->attach(Role::where('name', 'basic')->value('id'));

        $collection = Collection::factory()->create();

        $response = $this->actingAs($user)->deleteJson('/api/v1/collection/'.$collection->id);

        $response->assertStatus(403);
    }

    public function test_admin_user_can_access_deleting_collection(): void
    {
        $this->seed(RoleSeeder::class);
        $user = User::factory()->create();
        $user->roles()->attach(Role::where('name', 'admin')->value('id'));

        $collection = Collection::factory(5)->create();

        $response = $this->actingAs($user)->deleteJson('/api/v1/collection/'.$collection->last()->id);
        $response->assertStatus(200);

        $response = $this->get('/api/v1/collection');
        $response->assertStatus(200);
        $response->assertJsonCount(4, 'data');
    }
}
