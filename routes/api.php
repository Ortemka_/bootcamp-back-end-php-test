<?php

use App\Http\Controllers\Api\V1\Admin\CollectionController as AdminCollectionController;
use App\Http\Controllers\Api\V1\Admin\ContributorController as AdminContributorController;
use App\Http\Controllers\Api\V1\Auth\LoginController;
use App\Http\Controllers\Api\V1\CollectionController;
use App\Http\Controllers\Api\V1\ContributorController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::prefix('v1')->middleware(['auth:sanctum', 'role:admin'])->group(function () {
    Route::apiResource('collection', AdminCollectionController::class)
        ->only(['store', 'update', 'destroy']);
    Route::apiResource('contributor', AdminContributorController::class)
        ->only(['update', 'destroy']);
});

Route::prefix('v1')->group(function () {
    Route::apiResource('collection', CollectionController::class)
        ->only(['index', 'show']);
    Route::apiResource('contributor', ContributorController::class)
        ->only(['index', 'show', 'store']);
    Route::post('/collection/{collection}/add-amount', [CollectionController::class, 'addContributor']);
});

Route::post('/v1/login', LoginController::class);
