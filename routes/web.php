<?php

use App\Models\Role;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/setup', function () {
    $credentials = [
        'email' => 'user@user.com',
        'password' => 'password',
    ];

    if (! Auth::attempt($credentials)) {
        $user = new User();

        $user->name = 'User';
        $user->email = $credentials['email'];
        $user->password = Hash::make($credentials['password']);

        $user->save();
        $user->roles()->attach(Role::where('name', 'admin')->value('id'));

        if (Auth::attempt($credentials)) {
            $user = Auth::user();

            $adminToken = $user->createToken('new_user');

            return [
                'New user with admin role token' => $adminToken->plainTextToken,
            ];
        }
    } else {
        $user = User::where('email', $credentials['email'])->first();

        return response()->json([
            'User with admin role token' => $user->createToken('user_token')->plainTextToken,
        ]);
    }
});
