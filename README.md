# Collections Api

### Features
- Creating new collections using a POST request
- Getting a list of all fees using a GET request
- Adding to the collection of contributions using a POST request
- Retrieving details of a specific collection by its ID using a GET request
- Filtering collections by the amount left until the final amount is reached
- Get a list of collections that have contributions less than the target amount
- Editing and deleting collections and contributions
- Creating a new user using the artisan command - ( `artisan users:create` )

For test data run command: `artisan migrate:fresh --seed`

## Postman Rotes examples

- `Don't forget to ...`
- `Add Accept - application/json to the Headers`
- `POST, PUT, PATCH and DELETE requests require access.`
- `To get it, you need to follow the path - .../setup, example - http://localhost/setup`
- `Or use login route, details in section - POST Login `
- `Take the admin token and add it to Postman - Authorization - Bearer Token`

### GET

GET requests for all information about collections and contributors or about a specific one

| Request | Route Example  | Description |
| ------ | ------ | ------ |
| GET | ... /api/v1/collection       | Get information about all collections      |
| GET | ... /api/v1/collection/{id}  | Get information about specific collection  |
| GET | ... /api/v1/contributor      | Get information about all contributors     |
| GET | ... /api/v1/contributor/{id} | Get information about specific contributor |

### GET with filtering

GET requests for filtering collections

| Request | Route Example                               | Description |
| ------ |---------------------------------------------| ------ |
| GET | ... /api/v1/collection?notClosed=true       | Get a collections that have a contribution amount less than the target amount  |
| GET | ... /api/v1/collection?amountShortfall=1000 | Get a collections for the amount left until the final amount is reached  |



### POST

POST requests to create a new collection or contribution
Examples data for queries below

| Request | Route Example  | Description |
| ------ | ------ | ------ |
| POST | ... /api/v1/collection       | Create a new collection |
| POST | ... /api/v1/collection/{id}/add-amount  | Adding a contribution for a specific collection  |
| POST | ... /api/v1/contributor      | Create a new contributor  |

Body for collection POST
```sh
{
    "title": "Collection for ZSU",
    "description": "For 3 bayraktars",
    "target_amount": 4000,
    "link": "https://ru.wikipedia.org/wiki/Bayraktar_TB2"
}
```

Body for contributor POST
If use (... /api/v1/collection/{id}/add-amount) collection_id not needed
```sh
{
    "collection_id": 1,
    "user_name": "Name Surname",
    "amount": 1000,
}
```

### POST Login 
POST request to receive a token

| Request | Route Example     | Description |
| ------ |-------------------| ------ |
| POST | ... /api/v1/login | Receiving a token |

Body for login POST
```sh
{
    "email": "admin@admin.com",
    "password": "password",
}
```

### PUT and PATCH

PUT and PATCH requests to update existing collection or contribution
Examples data for queries below

`If we change the record using PUT, we must specify all the required fields.`

| Request | Route Example                | Description              |
| ------ |------------------------------|--------------------------|
| PUT / PATCH | ... /api/v1/collection/{id}  | Update existing collection    |
| PUT / PATCH | ... /api/v1/contributor/{id} | Update existing contributor |

Body for collection PUT / PATCH
```sh
{
    "title": "People's Bayraktar",
    "description": "Fundraising for bayraktars for ZSU",
    "target_amount": 5000,
    "link": "https://ru.wikipedia.org/wiki/Bayraktar_TB2"
}
```

Body for contributor PUT / PATCH
```sh
{
    "user_name": "Name Surname",
    "amount": 2000,
}
```
