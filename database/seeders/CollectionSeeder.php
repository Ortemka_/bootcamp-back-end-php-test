<?php

namespace Database\Seeders;

use App\Models\Collection;
use Illuminate\Database\Seeder;

class CollectionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Collection::factory()->count(3)->hasContributors(3)->create();
        Collection::factory()->count(5)->hasContributors(2)->create();
        Collection::factory()->count(2)->hasContributors(1)->create();
    }
}
