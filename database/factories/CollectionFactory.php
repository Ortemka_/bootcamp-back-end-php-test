<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Collection>
 */
class CollectionFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $wordsNumber = rand(3, 5);

        return [
            'title' => $this->faker->words($wordsNumber, true),
            'description' => $this->faker->text(),
            'target_amount' => $this->faker->numberBetween(100, 10000),
            'link' => $this->faker->url(),
        ];
    }
}
